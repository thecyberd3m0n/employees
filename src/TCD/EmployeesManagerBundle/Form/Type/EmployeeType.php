<?php

namespace TCD\EmployeesManagerBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use TCD\EmployeesManagerBundle\Form\Type\EmployeeImageType;

class EmployeeType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        
        $builder->add('name', 'text', array('required' => true))
                ->add('surname', 'text', array('required' => true))
                ->add('birthdate', 'birthday')
                ->add('email', 'email')
                ->add('image', new EmployeeImageType())
                ->add('pesel')
                ->add('job')->setMethod('POST')
                ->getForm();
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data-class' => 'TCDEmployeesManagerBundle:Entity:Employee',
            'cascade_validation' => true,
        ));
    }

    public function getName() {
        return 'employee';
    }

}
