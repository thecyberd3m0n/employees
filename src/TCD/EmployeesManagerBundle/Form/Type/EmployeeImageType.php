<?php

namespace TCD\EmployeesManagerBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EmployeeImageType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('file', 'file', array(
            "attr" => array(
                "accept" => "image/*",
                "property" => "file"
            )
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(
                array(
                    'data_class' => 'TCD\EmployeesManagerBundle\Entity\EmployeeImage',
                    'cascade_validation' => true
                )
        );
    }

    public function getName() {
        return 'file';
    }

}
