$(document).ready(function() {
    $('#MyGrid').datagrid({
        dataSource: new EmployeeDataSource({
            columns: [
                {
                    property: 'imageLink',
                    label: 'Image',
                    sortable: false
                }, {
                    property: 'name',
                    label: 'Name',
                    sortable: true
                }, {
                    property: 'surname',
                    label: 'Surname',
                    sortable: true
                }, {
                    property: 'email',
                    label: 'E-mail',
                    sortable: true
                }, {
                    property: 'job',
                    label: 'Job',
                    sortable: true
                }, {
                    property: 'id',
                    label: 'Options',
                    sortable: false
                }],
            formatter: function(items) {
                $.each(items, function(index, item) {
                    var deleteButton = $('<button />').css('float', 'right').text('Delete').attr('id', item.id).addClass("btn").click(function() {
                        removeEmployee($(this).attr('id'));
                    });

                    var editButton = $('<button />').css('float', 'left').text('Edit').attr('id', item.id).addClass("btn").click(function() {
                        editEmployee($(this)).attr('id');
                    });
                    var buttons = editButton.add(deleteButton);
                    item.id = buttons;
                    item.imageLink = "<img src=" + item.image.path + "/>";

                });
            }
        })

    });

    $("input[type='submit']").attr("disabled", false);

    $("form").submit(function() {
        $("input[type='submit']").attr("disabled", true).val("Please wait...");
        return true;
    });

    $('#add-user').click(function() {
        $('#addUserForm').dialog({
            modal: true,
            title: 'Add new employee',
            width: 1000,
            buttons: [
                {
                    text: 'Submit',
                    click: function() {
                        $(this).find('form').submit();
                        //location.reload();
                    }
                },
                {
                    text: 'Cancel',
                    click: function() {
                        $(this).dialog("close");
                    }
                }
            ]
        });
    });

});

function removeEmployee(id) {

    $("#prompt").text("Are you sure you want to delete him??").dialog({
        title: "Are you sure",
        buttons: [
            {
                text: "Cancel",
                click: function() {
                    $(this).dialog("close");
                }
            },
            {
                text: "Yes",
                click: function() {
                    var url = document.URL + 'employees/delete/' + id;
                    $.post(url, '', function() {
                        $("#MyGrid").datagrid("reload");
                        $('#prompt').dialog("close");
                    });
                }
            }
        ]
    });
}

