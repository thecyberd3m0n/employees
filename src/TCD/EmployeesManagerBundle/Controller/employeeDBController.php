<?php

namespace TCD\EmployeesManagerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use TCD\EmployeesManagerBundle\Entity\Employee;
use TCD\EmployeesManagerBundle\Entity\EmployeeImage;
use JMS\Serializer\SerializerBuilder;
use TCD\EmployeesManagerBundle\Form\Type\EmployeeType;
use TCD\EmployeesManagerBundle\Form\Type\EmployeeImageType;
use Doctrine\ORM\Query;

class employeeDBController extends Controller {

    public function getListJSONAction($page) {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('TCDEmployeesManagerBundle:Employee');
        $query = $em->createQuery('SELECT COUNT(u.id) FROM TCDEmployeesManagerBundle:Employee u');
        $total = $query->getSingleScalarResult();
        $articles_per_page = $this->container->getParameter('max_articles_on_page');
        $last_page = ceil($total / $articles_per_page);
        $previous_page = $page > 1 ? $page - 1 : 1;
        $next_page = $page < $last_page ? $page + 1 : $last_page;
        $entities = $repo->findAll();
        $serializer = SerializerBuilder::create()->build();
        $jsonContent = $serializer->serialize(array(
            'entities' => $entities,
            'last_page' => (int) $last_page,
            'previous_page' => (int) $previous_page,
            'current_page' => (int) $page,
            'next_page' => (int) $next_page,
            'total' => (int) $total
                ), 'json');
        $response = new Response($jsonContent);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function getEmployeeJSONAction($id) {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('TCDEmployeesManagerBundle:Employee');
        $employee = $repo->find($id);
        if (!$employee) {
            throw $this->createNotFoundException(
                    'No product found for id ' . $id
            );
        } else {
            $respArr = array(
                'id' => (int) $employee->getId(),
                'name' => $employee->getName(),
                'surname' => $employee->getSurname(),
                'email' => $employee->getEmail(),
                'image' => $employee->getImage(),
                'birthDate' => $employee->getBirthDate(),
                'pesel' => $employee->getPesel(),
                'job' => $employee->getJob()
            );
        }
        return new Response(json_encode($respArr));
    }

    //this function will be unused, I leave it here just to not forget how to do it ;)
    public function addEmployeeJSONAction(Request $request) {
        $employee = new Employee();
        $employee->setName($request->get('name'));
        $employee->setSurname($request->get('surname'));
        $employee->setBirthDate(new \DateTime($request->get('birthDate')));
        $employee->setPesel($request->get('pesel'));
        $employee->setJob($request->get('job'));
        $employee->setEmail($request->get('email'));
        $errors = $this->get('validator')->validate($employee);

        $em = $this->getDoctrine()->getManager();
        $em->persist($employee);
        $em->flush();

        return new Response('success');
    }

    public function editEmployeeAction($id) {
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();
        $employee = $em->getRepository('TCDEmployeesManagerBundle:Employee')->find($id);

        if (!$employee) {
            throw $this->createNotFoundException(
                    'No product found for id ' . $id
            );
        }


        $employeeImage = new EmployeeImage();
        $employee->setImage($employeeImage);
        $form = $this->createForm(new EmployeeType(), $employee);
        $form->handleRequest($request);
        $image = $form->getData()->getImage();
        
        if ($form->isValid()) {
            if (!is_null($image->getFile())) {
                $employeeImage->upload();
            } else {
                $employee->setImage(null);
            }
            $em->flush();
        } else {
            return $this->redirect($this->generateUrl('tcd_employees_manager_homepage'));
        }

        return $this->redirect($this->generateUrl('tcd_employees_manager_homepage'));
    }

    public function deleteEmployeeAction($id) {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('TCDEmployeesManagerBundle:Employee');
        if (is_null($user = $repo->find($id))) {
            return new Response('not exist');
        }
        $em->remove($user);
        $em->flush();
        return new Response('success');
    }

}
