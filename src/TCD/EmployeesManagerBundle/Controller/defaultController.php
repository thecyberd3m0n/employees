<?php

namespace TCD\EmployeesManagerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use TCD\EmployeesManagerBundle\Entity\Employee;
use TCD\EmployeesManagerBundle\Entity\EmployeeImage;
use TCD\EmployeesManagerBundle\Form\Type\EmployeeType;
use Symfony\Component\HttpFoundation\Request;

class defaultController extends Controller {

    public function indexAction(Request $request) {
        if (false === $this->get('security.context')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException();
        }
        $em = $this->getDoctrine()->getManager();

        $employee = new Employee();
        $employeeImage = new EmployeeImage();

        $employee->setImage($employeeImage);
        $form = $this->createForm(new EmployeeType(), $employee);
        

        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $employeeImage->upload();
                $em->persist($employee);
                $em->flush();
            } else {
                return $this->render('TCDEmployeesManagerBundle:Default:index.html.twig', array('form' => $form->createView(), 'errors' => $form->getErrors()));
            }
        }

        return $this->render('TCDEmployeesManagerBundle:Default:index.html.twig', array('form' => $form->createView()));
    }

}
