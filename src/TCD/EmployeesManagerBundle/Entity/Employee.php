<?php

namespace TCD\EmployeesManagerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use TCD\EmployeesManagerBundle\Entity\EmployeeImage;

/**
 * Employee
 *
 * @ORM\Table(name="Employee")
 * @ORM\Entity
 */
class Employee {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthDate", type="date")
     */
    private $birthDate;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="surname", type="string", length=255)
     * @Assert\NotBlank
     */
    private $surname;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     * @Assert\NotBlank
     */
    private $email;

    /**
     * @Assert\Type(type="TCD\EmployeesManagerBundle\Entity\EmployeeImage")
     * @ORM\OneToOne(targetEntity="EmployeeImage",mappedBy="id",fetch="EAGER",cascade={"all"})
     * @ORM\JoinColumn(name="imageID", referencedColumnName="id",nullable=TRUE)
     * 
     * 
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="pesel", type="string", length=255)
     */
    private $pesel;

    /**
     * @var string
     *
     * @ORM\Column(name="job", type="string", length=255)
     * @Assert\NotBlank
     */
    private $job;

    /**
     * Get id
     *
     * @return integer 
     * @Assert\NotBlank
     * 
     */
    public function getId() {
        return $this->id;
    }
    
    public function getImage() {
        return $this->image;
    }

    public function setImage(EmployeeImage $image = null) {
        $this->image = $image;
        return $this;
    }

    /**
     * Set birthDate
     *
     * @param \DateTime $birthDate
     * @return Employee
     */
    public function setBirthDate($birthDate) {
        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * Get birthDate
     *
     * @return \DateTime 
     */
    public function getBirthDate() {
        return $this->birthDate;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Employee
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set surname
     *
     * @param string $surname
     * @return Employee
     */
    public function setSurname($surname) {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname
     *
     * @return string 
     */
    public function getSurname() {
        return $this->surname;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Employee
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Set pesel
     *
     * @param string $pesel
     * @return Employee
     */
    public function setPesel($pesel) {
        $this->pesel = $pesel;

        return $this;
    }

    /**
     * Get pesel
     *
     * @return string 
     */
    public function getPesel() {
        return $this->pesel;
    }

    /**
     * Set job
     *
     * @param string $job
     * @return Employee
     */
    public function setJob($job) {
        $this->job = $job;

        return $this;
    }

    /**
     * Get job
     *
     * @return string 
     */
    public function getJob() {
        return $this->job;
    }

}
