<?php

namespace TCD\LoginBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;

class DefaultController extends Controller {

    public function loginAction($logout = false) {
        $messages = array();
        if ($logout == true) {
            array_push($messages, 'Wylogowano pomyślnie');
        }

        $request = $this->getRequest();
        $session = $request->getSession();

        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $errors = $request->attributes->get(
                    SecurityContext::AUTHENTICATION_ERROR
            );
        } else {
            $errors = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
            
        }

        return $this->render('LoginBundle:Default:index.html.twig', array(
                    'messages' => $messages,
                    'last_username' => $session->get(SecurityContext::LAST_USERNAME),
                    'errors' => $errors));
    }

    public function logoutAction() {
        return $this->loginAction(true);
    }

}
