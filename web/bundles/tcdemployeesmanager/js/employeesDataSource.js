//script created by this example: https://github.com/adamalex/fuelux-dgdemo/blob/master/datasource.js

var EmployeeDataSource = function(options) {
    this._formatter = options.formatter;
    this._columns = options.columns;
};

EmployeeDataSource.prototype = {
    columns: function() {

        return this._columns;
    },
    data: function(options, callback) {
        var url = document.URL + 'employees';
        var self = this;
        
        

        if (typeof options.page === 'undefined')
        {
            options.page = 1;
        }
            url += '/' + options.page; //page number
        //TODO: add functionality for difrent page size
            $.ajax(url, {
                dataType: 'json',
                type: 'GET'

            }).done(function(response) {
                //prepare data
                var per_page = 10; //temporarily static
                var data = response.entities;
                var count = data.length;
                var startIndex = (response.current_page - 1) * per_page;
                var endIndex = startIndex + per_page;
                var end = (endIndex > count) ? count : endIndex;
            var pages = Math.ceil(response.total / per_page);
                var page = response.current_page;
                var start = startIndex + 1;

                data = data.slice(startIndex, endIndex);
                // Allow client code to format the data
                if (self._formatter)
                    self._formatter(data);

                // Return data to Datagrid
                callback({data: data, start: start, end: end, count: count, pages: pages, page: page});
            });
        
        
    }
};

function countProperties(obj) {
    var count = 0;

    for (var prop in obj) {
        if (obj.hasOwnProperty(prop))
            ++count;
    }

    return count;
}