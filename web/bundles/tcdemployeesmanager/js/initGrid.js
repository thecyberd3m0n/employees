function editEmployee(item, id) {
    var birthDate = new Date(item.birth_date);
    $('form[name="employee"]').attr('action', 'employees/edit/' + id);

    var form = $('form[name="employee"]');
    $('input#employee_name').val(item.name);
    $('input#employee_surname').val(item.surname);
    $('input#employee_email').val(item.email);

    $('select#employee_birthdate_month option').filter(function() {
        return $(this).val() == birthDate.getMonth();
    }).prop('selected', true);
    $('select#employee_birthdate_day option').filter(function() {
        return $(this).val() == birthDate.getDay();
    }).prop('selected', true);
    $('select#employee_birthdate_year option').filter(function() {
        return $(this).val() == birthDate.getYear();
    }).prop('selected', true);
    $('input#employee_pesel').val(item.pesel);
    $('input#employee_job').val(item.job);


    $('#addUserForm').dialog({
        modal: true,
        title: 'Edit employee ' + item.name + ' ' + item.surname,
        beforeClose: function(event, ui) {
            $('form[name="employee"]').attr('action', '').find('input').val('');
        },
        width: 600,
//        buttons: [
//            {
//                text: 'Submit',
//                click: function() {
//                    $(this).find('form').submit();
//                }
//            },
//            {
//                text: 'Cancel',
//                click: function() {
//                    $(this).dialog("close");
//                }
//            }
//        ]

    });

}

function removeEmployee(id) {

    $("#prompt").text("Are you sure you want to delete him??").dialog({
        title: "Are you sure",
        buttons: [
            {
                text: "Cancel",
                click: function() {
                    $(this).dialog("close");
                }
            },
            {
                text: "Yes",
                click: function() {
                    var url = document.URL + 'employees/delete/' + id;
                    $.post(url, '', function() {
                        $("#MyGrid").datagrid("reload");
                        $('#prompt').dialog("close");
                    });
                }
            }
        ]
    });
}

$(document).ready(function() {

    $('#MyGrid').datagrid({
        dataSource: new EmployeeDataSource({
            columns: [
                {
                    property: 'imageLink',
                    label: 'Image',
                    sortable: false
                }, {
                    property: 'name',
                    label: 'Name',
                    sortable: true
                }, {
                    property: 'surname',
                    label: 'Surname',
                    sortable: true
                }, {
                    property: 'email',
                    label: 'E-mail',
                    sortable: true
                }, {
                    property: 'job',
                    label: 'Job',
                    sortable: true
                }, {
                    property: 'id',
                    label: 'Options',
                    sortable: false
                }],
            formatter: function(items) {

                $.each(items, function(index, item) {
                    var deleteButton = $('<button />').css('float', 'right').text('Delete').attr('id', item.id).addClass("btn").click(function() {
                        removeEmployee($(this).attr('id'));
                    });

                    var editButton = $('<button />').css('float', 'left').text('Edit').attr('id', item.id).addClass("btn").click(function() {
                        editEmployee(item, $(this).attr('id'));
                    });
                    var buttons = editButton.add(deleteButton);
                    item.id = buttons;
                    if (!(typeof item.image === 'undefined')) {
                        item.imageLink = "<img src=/web/" + item.image.path + " style=\"width:150px\"/>";
                    }

                });
            }
        })

    });

    $("input[type='submit']").attr("disabled", false);

    $("form").submit(function() {
        $("input[type='submit']").attr("disabled", true).val("Please wait...");
        return true;
    });

    $('#add-user').click(function() {
        $('#addUserForm').dialog({
            modal: true,
            title: 'Add new employee',
            width: 600,
//            buttons: [
//                {
//                    text: 'Submit',
//                    click: function() {
//                        $(this).find('form').submit();
//                        //location.reload();
//                    }
//                },
//                {
//                    text: 'Cancel',
//                    click: function() {
//                        $(this).dialog("close");
//                    }
//                }
//            ]
        });
    });
    $('#datagrid-reload').click(function() {
        $("#MyGrid").datagrid("reload");
    });
});

